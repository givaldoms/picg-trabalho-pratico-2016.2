//
// Created by antonio on 04/03/17.
//

#ifndef PICG_2016_2_PLANTA_H
#define PICG_2016_2_PLANTA_H

#include "Animal.h"

#include <GL/gl.h>
#include <GL/freeglut.h>
#include <iostream>
#include <vector>

using namespace std;


class Planta {

private:
    int posicaoX;
    int posicaoZ;
    float massa;
    float escala;

private:
    int taxaCrescimento;
    Modelo *modelo;


public:

    Planta(int taxaCrescimento);

    bool gerar(vector<vector<char>> &mundo);

    void setPosicaoX(int sentidoX);

    void setPosicaoZ(int sentidoZ);

    void desenha(vector<vector<char>> &mundo);

    int getTaxaCrescimento() const;

    float getMassa() const;

    void setMassa(float massa);

};


#endif //PICG_2016_2_PLANTA_H
