//
// Created by antonio on 05/03/17.
//

#ifndef PICG_2016_2_LUZ_H
#define PICG_2016_2_LUZ_H

#include <vector>

using namespace std;

/**
 * @param luz1 ligar ou desligar luz1
 *
 */

class Luz {
private:
    int luz1;
    int luz2;
public:
    Luz(int x, int y, int altura, vector<vector<char>> &mundo);

    void iluminar(int x, int y, int autura, vector<vector<char>> &mundo);

    void cLuz1();

    void cLuz2(int altura, vector<vector<char>> &mundo);

    void desenharLuz(int altura);

    int getluz1() const;

    void setluz1(int luz1);

    int getluz2() const;

    void setluz2(int luz2);

    void propriedade();
};


#endif //PICG_2016_2_LUZ_H