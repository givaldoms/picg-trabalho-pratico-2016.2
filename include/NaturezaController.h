//
// Created by givaldo on 27/02/17.
//

#ifndef PICG_2016_2_NATUREZACONTROLLER_H
#define PICG_2016_2_NATUREZACONTROLLER_H

#include "../lib/utils/Cor.cpp"
#include "Animal.h"

using namespace std;


class NaturezaController {
    float porcentagemIlha;
    Modelo *modeloChao;

public:
    NaturezaController(float porcentagemIlha);

    int getAreaIlha(vector<vector<char>> mundo);

    void desenhaVerticesCentrais(vector<vector<char>> &mundo);

    void geraIlha(vector<vector<char>> &mundo);

    void geraOceano(vector<vector<char>> &mundo);

    void desenhaNatureza(vector<vector<char>> mundo);

    Cor getItemCor(char item);

};


#endif //PICG_2016_2_NATUREZACONTROLLER_H
