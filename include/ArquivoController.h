//
// Created by givaldo on 05/03/17.
//

#ifndef PICG_2016_2_ARQUIVOCONTROLLER_H
#define PICG_2016_2_ARQUIVOCONTROLLER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>


using namespace std;

class ArquivoController {
    vector<string> cena;

    vector<string> ilha;
    vector<string> lagos;

    vector<string> leoes;
    vector<string> zebras;
    vector<string> plantas;

public:

    ArquivoController();

    void CarregarArquivo(string nomeArquivo);

    vector<string> split(const string &s, char delim);

    const vector<string> &getCena() const;

    const vector<string> &getIlha() const;

    const vector<string> &getLagos() const;

    const vector<string> &getLeoes() const;

    const vector<string> &getZebras() const;

    const vector<string> &getPlantas() const;

};


#endif //PICG_2016_2_ARQUIVOCONTROLLER_H
