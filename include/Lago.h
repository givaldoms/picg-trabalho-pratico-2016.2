#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
#ifndef PICG_2016_2_LAGO_H
#define PICG_2016_2_LAGO_H

#include <vector>

using namespace std;

class Lago {

private:
    int x1;
    int z1;
    int x2;
    int z2;

public:

    bool cresceLago(vector<vector<char>> &mundo, int tamMax);

    Lago(int x0, int z0);

    long getArea();

    Lago();

    int getX1() const;

    void setX1(int x1);

    int getX2() const;

    void setX2(int x2);

    int getZ1() const;

    void setZ1(int z1);

    int getZ2() const;

    void setZ2(int z2);

    void gerarLago(vector<vector<char>> &mundo);
};

#endif //PICG_2016_2_LAGO_H

#pragma clang diagnostic pop