//
// Created by Givaldo Marques on 22/02/17.
//

#ifndef PICG_2016_2_COR_H
#define PICG_2016_2_COR_H


#include <GL/gl.h>

class Cor {
    GLubyte r;
    GLubyte g;
    GLubyte b;

public:
    GLubyte getR() const;

    GLubyte getG() const;

    GLubyte getB() const;

    Cor(GLubyte r, GLubyte g, GLubyte b);

    Cor();
};


#endif //PICG_2016_2_COR_H
