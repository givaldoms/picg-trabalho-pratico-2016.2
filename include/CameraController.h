//
// Created by givaldo on 23/02/17.
//

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
#ifndef PICG_2016_2_TECLADOCONTROLLER_H
#define PICG_2016_2_TECLADOCONTROLLER_H


using namespace std;

class CameraController {
private:
    float obx;
    float oby;
    float obz;

    float alx;
    float aly;
    float alz;
    float cameraVelocidade;

public:
    CameraController(vector<vector<char>> mundo);

    float getObx() const;

    void setObx(float obx);

    float getOby() const;

    void setOby(float oby);

    float getObz() const;

    void setObz(float obz);

    float getAlx() const;

    void setAlx(float alx);

    float getAly() const;

    void setAly(float aly);

    float getAlz() const;

    void setAlz(float alz);

    float getCameraVelocidade() const;

    void setCameraVelocidade(float cameraVelocidade);
};


#endif //PICG_2016_2_TECLADOCONTROLLER_H

#pragma clang diagnostic pop