#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

#ifndef PICG_2016_2_ANIMAL_H
#define PICG_2016_2_ANIMAL_H

#include <iostream>
#include <vector>
#include "Cor.h"
#include "../lib/utils/CarregarObjeto.cpp"


using namespace std;

class Animal {

private:
    bool estaBrigando;
    bool estaVivo;

private:
    float velocidade;
    float sentidoX;
    float sentidoZ;
    int direcao;
    Modelo *modelo;
    int tipo;
    int taxaDiminuicao;
    float massa;

    Cor cor;

public:

    Animal();

    ~Animal();

    bool isEstaVivo() const;

    void setEstaVivo(bool estaVivo);

    int getTipo() const;

    void setTipo(int tipo);

    bool isEstaBrigando() const;

    void setEstaBrigando(bool estaBrigando);

    Modelo *getModelo() const;

    void setModelo(Modelo *modelo);

    int getTaxaDiminuicao() const;

    void setTaxaDiminuicao(int taxaDiminuicao);

    float getMassa() const;

    void setMassa(float massa);

    Animal(short tipo, int taxaDiminuicao);

    bool gerar(vector<vector<char>> &mundo);

    void movimentar(vector<vector<char>> &mundo);

    float getVelocidade() const;

    void setVelocidade(float velocidade);

    const Cor &getCor() const;

    void setCor(const Cor &cor);

    float getSentidoX() const;

    void setSentidoX(float sentidoX);

    float getSentidoZ() const;

    void setSentidoZ(float sentidoZ);

    int getDirecao() const;

    void setDirecao(int direcao);

    void desenha(vector<vector<char>> &mundo, int angulo);

    char getColisao(vector<vector<char>> mundo, int x, int z);

    void reproduzirLeao(vector<vector<char>> &mundo, vector<Animal> &leoes, int nLeoes);

    void reproduzirZebra(vector<vector<char>> &mundo, vector<Animal> &zebras, int nLeoes);


};


#endif //PICG_2016_2_ANIMAL_H

#pragma clang diagnostic pop