//
// Created by givaldo on 23/02/17.
//

#include <vector>
#include <string>
#include <cstdlib>

#include "../lib/models/Animal.cpp"
#include "../lib/controllers/CameraController.cpp"
#include "../lib/controllers/NaturezaController.cpp"
#include "../lib/controllers/ArquivoController.cpp"
#include "../lib/models/Planta.cpp"
#include "../lib/models/Luz.cpp"

#ifndef PICG_2016_2_MAIN_H
#define PICG_2016_2_MAIN_H

#endif //PICG_2016_2_MAIN_H

using namespace std;

void init(void);

void display(void);

void desenhaIlha(vector<vector<char>> &mundo, float pocentegemIlha);

void desenhaOceano(vector<vector<char>> &mundo);

//função que incrementa o ângulo de rotação
void spinDisplay(void);

//função chamada quando a tela é redimensionada
void reshape(int w, int h);

//evento que trata clique do mouse
void mouse(int button, int state, int x, int y);

//evento que trata do teclado em especial as setinha
void keySpecial(int key, int x, int y);

//evento que trata do teclado em especial w,a,s,d
void keyPressed(unsigned char key, int x, int y);

void imprimeMundo(vector<vector<char>> mundo);

void criarLagos(float porcentagemLagos);

void criarAnimais(int nAnimais, short tipo, int taxaDiminuicao);

void criarPlantas(int nPlantas, int taxaCrescimento);

float porcentagemZebraSobreviver(Animal *zebra, Animal *leao);

int getZebraBrigando();

Animal getLeaoBrigando();

void desenhaCeu(int cenaY);


void reproduzirPlanta(vector<vector<char>> &mundo, int i);
