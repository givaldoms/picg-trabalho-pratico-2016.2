//
// Created by givaldo on 22/02/17.
//

#include "../../include/Cor.h"

GLubyte Cor::getR() const {
    return r;
}

GLubyte Cor::getG() const {
    return g;
}

GLubyte Cor::getB() const {
    return b;
}

Cor::Cor(GLubyte r, GLubyte g, GLubyte b) : r(r), g(g), b(b) {}

Cor::Cor() {
    this->r = 0;
    this->g = 0;
    this->b = 0;
}
