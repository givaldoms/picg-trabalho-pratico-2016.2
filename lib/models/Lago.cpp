#include "../../include/Lago.h"

#include <GL/gl.h>
#include <iostream>

using namespace std;

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

Lago::Lago() {
}

Lago::Lago(int x1, int z1) {
    this->x1 = x1;
    this->z1 = z1;
    this->x2 = x1 + 1;
    this->z2 = z1 + 1;
}

/**
 *  Gera na matriz mundo as posições relacionadas ao lago
 *
 * @param matriz mundo
 */
void Lago::gerarLago(vector<vector<char>> &mundo) {

    for (int i = x1; i < x2; i++) {
        for (int j = z1; j < z2; j++) {
            mundo[i][j] = '*';
        }
    }
}

/**
 *
 * @param mundo matriz mundo
 * @param tamMax tamanho máximo que um lago pode ter
 * @return pode crescer o lago
 */
bool Lago::cresceLago(vector<vector<char>> &mundo, int tamMax) {

    int x1 = this->x1 - 1;
    int x2 = this->x2 + 1;
    int z1 = this->z1 - 1;
    int z2 = this->z2 + 1;

    //Se esse lago atingir o tamanho maximo definido
    if (this->getArea() >= tamMax) {
        return false;
    }

    //se a próxima coordenada a crescer não fizer parte da ilha
    if (mundo[x1][z1] != 'i' ||
        mundo[x2][z2] != 'i') {
        return false;
    }

    this->x1 = x1;
    this->z1 = z1;
    this->x2 = x2;
    this->z2 = z2;

    return true;
}


int Lago::getX1() const {
    return x1;
}

void Lago::setX1(int x1) {
    Lago::x1 = x1;
}

int Lago::getX2() const {
    return x2;
}

void Lago::setX2(int x2) {
    Lago::x2 = x2;
}

int Lago::getZ1() const {
    return z1;
}

void Lago::setZ1(int z1) {
    Lago::z1 = z1;
}

int Lago::getZ2() const {
    return z2;
}

void Lago::setZ2(int z2) {
    Lago::z2 = z2;
}

long Lago::getArea() {
    return (x2 - x1) * (z2 - z1);
}


