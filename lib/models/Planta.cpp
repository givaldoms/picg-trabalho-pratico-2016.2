//
// Created by Antônio Neto e Givaldo Marques
// on 04/03/17.
//

/**
 * Classe que define a posição da planta e desenha ela
 */

#include "../../include/Planta.h"

using namespace std;

/**
 * @param taxaCrescimento define a taxa crescimento
 * @massa define uma massa fixa ininical
 */

Planta::Planta(int taxaCrescimento) {
    this->taxaCrescimento = taxaCrescimento;
    this->massa = 10;
    this->posicaoX = 0;
    this->posicaoZ = 0;
    this->escala = 10;

    this->modelo = Modelo::carregarObj(
            (char *) "../modelos/grama/grama.obj",
            (char *) "../modelos/grama/grama.bmp");

};

/*Planta::Planta(int x, int z){
    posicaoX = x;
    posicaoZ = z;
}*/

/**
 * @param mundo
 * define onde a planta vai ser desenhada aleatoriamente
 */


bool Planta::gerar(vector<vector<char>> &mundo) {
    int x = this->posicaoX;
    int z = this->posicaoZ;

    if (mundo[x][z] != 'i') {
        return false;
    }

    mundo[x][z] = 'P';
    return true;
}

/**
 * desenha a planta e define o modelo
 */

void Planta::desenha(vector<vector<char>> &mundo) {
    // glEnable(GL_LIGHTING);

    this->massa += ((float) this->taxaCrescimento) / 100;

    for (int i = this->posicaoX - 1; i < this->posicaoX + 1; ++i) {
        for (int j = this->posicaoZ - 1; j < this->posicaoZ + 1; ++j) {
            if (mundo[i][j] == 'Z') {
                this->massa -= 5;
            }
        }
    }

    glPushMatrix();
    glColor3ub(100, 255, 100);

    glTranslatef((GLfloat) (this->posicaoX + 0.5), 0.0, (GLfloat) (this->posicaoZ + 0.5));

    this->escala = (this->massa / 150);

    glScalef(this->escala, this->escala, this->escala);

    //    glutSolidCube(1.0);
    glEnable(GL_TEXTURE_2D);
    this->modelo->desenhar();
    glDisable(GL_TEXTURE_2D);


    glPopMatrix();
    glPopAttrib();
    // glDisable(GL_LIGHTING);


}

void Planta::setPosicaoX(int PosicaoX) {
    Planta::posicaoX = PosicaoX;
}

void Planta::setPosicaoZ(int PosicaoZ) {
    Planta::posicaoZ = PosicaoZ;
}

int Planta::getTaxaCrescimento() const {
    return taxaCrescimento;
}


float Planta::getMassa() const {
    return massa;
}

void Planta::setMassa(float massa) {
    Planta::massa = massa;
}
