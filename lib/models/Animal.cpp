#include "../../include/Animal.h"

#include <GL/freeglut.h>
#include <ctime>


using namespace std;

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

Animal::Animal(short tipo, int taxaDiminuicao) {
    srand((unsigned int) time(NULL));

    this->velocidade = 0.1;
    this->sentidoX = 0;
    this->sentidoZ = 0;
    this->cor = cor;
    this->estaBrigando = false;
    this->estaVivo = true;
    this->direcao = rand() % 2;
    this->taxaDiminuicao = taxaDiminuicao;
    this->tipo = tipo;

    if (tipo == 1) {//leao
        this->massa = 1500;

        this->modelo = Modelo::carregarObj(
                (char *) "../modelos/leao/leao.obj",
                (char *) "../modelos/leao/leao.bmp");

    } else {//zebra
        this->massa = 1000;

        this->modelo = Modelo::carregarObj(
                (char *) "../modelos/zebra/zebra.obj",
                (char *) "../modelos/zebra/zebra.bmp");

    }
}

Animal::Animal() {}

/**
 *
 * @param mundo matriz Mundo
 * @return Pode desenhar nas coordenadas (x, z), ou  seja, se mundo[x][y] perterce a ilha
 */
bool Animal::gerar(vector<vector<char>> &mundo) {
    int x = (int) this->sentidoX;
    int z = (int) this->sentidoZ;

    if (mundo[x][z] != 'i') {// || mundo[x - 2][z - 2] == '~' || mundo[x + 2][z + 2] ==
        //                                        '~') {
        return false;
    }

    this->tipo == 1 ? mundo[x][z] = 'L' : mundo[x][z] = 'Z';
    return true;
}

/**
 *
 * Movimenta o animal.
 * Se houver colisão deve mudar a direção.
 * Aleariamente pode mudar de direção.
 *
 * @param mundo matiz mundo
 * @param tamMundo  tamanho do mundo
 *
 */
void Animal::movimentar(vector<vector<char>> &mundo) {

    srand((unsigned int) time(NULL));

    int x = (int) this->sentidoX;
    int z = (int) this->sentidoZ;

    int angulo;
    char colisao;
    this->massa -= taxaDiminuicao / 8;
    this->estaBrigando = false;

    if (this->direcao == 0) {//movimento na direção x

        if (this->velocidade >= 0) {//Animal indo para a direita
            colisao = getColisao(mundo, x + 1, z);
            angulo = 90;
        } else {//Animal indo para a esquerda
            colisao = getColisao(mundo, x - 1, z);
            angulo = 270;
        }

        if (colisao == '~') {//colisão com o oceano
            this->velocidade *= -1;//anda para o lado inverso
            this->sentidoX += this->velocidade;

        } else if (colisao == '*') {//colisão com o lago
            this->velocidade *= -1;//anda para o lado inverso
            this->massa += 10;//aumenta a massa do animal
            this->sentidoX += this->velocidade;//movimenta o animal

        } else if (colisao == 'P') {//colisão com uma planta
            if (this->tipo == 0) {//se for uma zebra
                this->massa += 10;//aumenta sua massa
            }

        } else if (colisao == 'L') {//colisão com um leão
            if (this->tipo == 1) {//lesão colidiu com outro leão
                this->velocidade *= -1;//anda para o lado inverso

            } else {
                this->estaBrigando = true;//detectou uma briga
            }

        } else if (colisao == 'Z') {
            if (this->tipo == 1) {//colisão com o uma zebra
                this->estaBrigando = true;//detectou uma briga


            } else {//zebra colidiu com outra zebra
                this->velocidade *= -1;//anda para o lado inverso
            }
        }

        //movimente o animal na direção relativa ao sentido
        //ou seja, a direita se velocidade > 0 e esquerda se
        //velocidade < 0
        this->sentidoX += this->velocidade;

    } else {//movimento na direção z

        if (this->velocidade >= 0) {//Animal indo para a cima
            colisao = getColisao(mundo, x, z + 1);
            angulo = 0;
        } else {//Animal indo para a baixo
            colisao = getColisao(mundo, x, z - 1);
            angulo = 180;
        }

        if (colisao == '~') {//colisão com o oceano
            this->velocidade *= -1;
            this->sentidoZ += this->velocidade;

        } else if (colisao == '*') {//colisão com o lago
            this->velocidade *= -1;//anda para o lado inverso
            this->massa += 50;//aumenta a massa do animal
            this->sentidoZ += this->velocidade;//movimenta o animal

        } else if (colisao == 'P') {//colisão com uma planta
            if (this->tipo == 0) {//se for uma zebra
                this->massa += 100;//aumenta sua massa
            }

        } else if (colisao == 'L') {//colisão com uma leão
            if (this->tipo == 1) {//colidiu com outro leão
                this->velocidade *= -1;//muda de direção

            } else {//dedectou uma briga
                // cout << "zebra brigou" << endl;
                this->estaBrigando = true;
            }

        } else if (colisao == 'Z') {//colidiu com uma zebra
            if (this->tipo == 1) {//detectou uma briga
                // cout << "leao brigou" << endl;
                this->estaBrigando = true;


            } else {// colidiu com outra zebra
                this->velocidade *= -1;//inverte a velocidade
            }
        }

        //movimente o animal na direção relativa ao sentido
        //ou seja, a direita se velocidade > 0 e esquerda se
        //velocidade < 0
        this->sentidoZ += this->velocidade;
    }

    this->direcao = (int) (random() % 2);

    mundo[x][z] = 'i';
    desenha(mundo, angulo);

}

/*

void Animal::desenha(vector<vector<char>> &mundo) {

    if(this->tipo == 1){
        mundo[sentidoX][sentidoZ] = 'L';
        glColor3ub(215, 215, 12);
    }else{
        glColor3ub(128, 128, 128);
        mundo[sentidoX][sentidoZ] = 'Z';
    }


    //glLoadIdentity();
    //glRotatef(45.0f,0.0f,1.0f,0.0f);

    glTranslatef(this->sentidoX, 0.0, (GLfloat) (this->sentidoZ + 0.5));
    //glScalef(0.002, 0.002, 0.002);
    //glPushAttrib(GL_CURRENT_BIT);
    glutSolidCube(1);
    //this->modelo->desenhar();
    glPopAttrib();


}

*/

/**
 * Desenha o animal na tela usando modelos
 *
 * @param mundo Matriz mundo
 */
void Animal::desenha(vector<vector<char>> &mundo, int angulo) {

    //glLoadIdentity();
    //glRotatef(45.0f,0.0f,1.0f,0.0f);

    float x = (int) this->sentidoX;
    float z = (int) this->sentidoZ;

    glPushMatrix();
    glTranslatef(sentidoX, 0.0, sentidoZ);
    float escala;


    if (this->tipo == 1) {
        mundo[sentidoX][sentidoZ] = 'L';

        escala = (massa / 60000);

        glScalef(escala, escala, escala);
        glRotated(angulo, 0, 1, 0);

    } else {
        glRotated(270, 1, 0, 0);
        glRotated(angulo, 0, 0, 1);

        mundo[sentidoX][sentidoZ] = 'Z';

        escala = (massa / 1000);

        glScalef(escala, escala, escala);
    }
    GLfloat mat_specular[] = {0.0, 0.0, 0.0};
    GLfloat mat_diffuse[] = {0.8, 0.8, 0.8};
    GLfloat mat_ambient[] = {0.2, 0.2, 0.2};
    GLfloat mat_shininess[] = {0.0};

    glEnable(GL_TEXTURE_2D);
    glPushMatrix();
    glPushAttrib(GL_LIGHTING_BIT);
    glTranslatef((GLfloat) (x + 0.5), 0.0, (GLfloat) (z + 0.5));
    glPopMatrix();
    //como o animal interage com a luz
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_ambient);
    //
    glEnable(GL_LIGHTING);
    this->modelo->desenhar();
    glDisable(GL_LIGHTING);
    glPopAttrib();
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);
    glutPostRedisplay();

}


/**
 * Gera um número definido de leões próximos a outro leão passado por parâmetro
 * @param mundo Matriz mundo
 * @param leoes Animal que irá gerar outros animais
 * @param r número de animais a serem gerados
 */
void Animal::reproduzirLeao(vector<vector<char>> &mundo, vector<Animal> &leoes, int r) {

    this->setMassa(this->getMassa() / r);//Divide sua massa entre todos

    int aCriados = 0;//quantidade de animais criados
    int x = (int) this->getSentidoX();//posição atual em x
    int z = (int) this->getSentidoZ();//posição atual em y


    for (int i = x - 1; i < x + 1; i++) {
        if (i <= 0 || i >= mundo.size() - 1) {
            break;
        }
        for (int j = z - 1; j < z + 1; j++) {
            if (i <= 0 || i >= mundo[0].size() - 1) {
                break;
            }

            //se foram criados todos os animais
            if (aCriados == r) {
                return;
            }

            if (mundo[i][j] == 'i') {//se há ilha em mundo[x][z]
                mundo[i][j] = 'L';//cria um animal nesta posição
                Animal a = *new Animal(1, this->getTaxaDiminuicao());
                a.setSentidoX(i);
                a.setSentidoZ(j);
                a.setMassa(this->getMassa() / (r + 1));
                leoes.push_back(a);//insere no array de leões
                aCriados++;//incrementa o contador
            }

        }
    }

}


/**
 * Gera um número definido de leões próximos a outro leão passado por parâmetro
 * @param mundo Matriz mundo
 * @param leoes Animal que irá gerar outros animais
 * @param r número de animais a serem gerados
 */
void Animal::reproduzirZebra(vector<vector<char>> &mundo, vector<Animal> &zebras, int r) {

    this->setMassa(this->getMassa() / r);//Divide sua massa entre todos

    int aCriados = 0;//quantidade de animais criados
    int x = (int) this->getSentidoX();//posição atual em x
    int z = (int) this->getSentidoZ();//posição atual em y

    for (int i = x - 1; i < x + 1; i++) {
        if (i <= 0 || i >= mundo.size() - 1) {
            break;
        }
        for (int j = z - 1; j < z + 1; j++) {
            if (i <= 0 || i >= mundo[0].size() - 1) {
                break;
            }
            //se foram criados todos os animais
            if (aCriados == r) {
                return;
            }

            if (mundo[i][j] == 'i') {//se há ilha em mundo[x][z]
                mundo[i][j] = 'L';//cria um animal nesta posição
                Animal a = *new Animal(0, this->getTaxaDiminuicao());
                a.setSentidoX(i);
                a.setSentidoZ(j);
                a.setMassa(this->getMassa() / (r + 1));
                zebras.push_back(a);//insere no array de leões
                aCriados++;//incrementa o contador
            }

        }
    }
}

char Animal::getColisao(vector<vector<char>> mundo, int x, int z) {
    return mundo[x][z];
}


const Cor &Animal::getCor() const {
    return cor;
}

void Animal::setCor(const Cor &cor) {
    Animal::cor = cor;
}

float Animal::getSentidoX() const {
    return sentidoX;
}

void Animal::setSentidoX(float sentidoX) {
    Animal::sentidoX = sentidoX;
}

float Animal::getSentidoZ() const {
    return sentidoZ;
}

void Animal::setSentidoZ(float sentidoZ) {
    Animal::sentidoZ = sentidoZ;
}

int Animal::getDirecao() const {
    return direcao;
}

void Animal::setDirecao(int direcao) {
    Animal::direcao = direcao;
}

Modelo *Animal::getModelo() const {
    return modelo;
}

void Animal::setModelo(Modelo *modelo) {
    Animal::modelo = modelo;
}

int Animal::getTaxaDiminuicao() const {
    return taxaDiminuicao;
}

void Animal::setTaxaDiminuicao(int taxaDiminuicao) {
    Animal::taxaDiminuicao = taxaDiminuicao;
}

float Animal::getMassa() const {
    return massa;
}

void Animal::setMassa(float massa) {
    Animal::massa = massa;
}


int Animal::getTipo() const {
    return tipo;
}

void Animal::setTipo(int tipo) {
    Animal::tipo = tipo;
}

bool Animal::isEstaBrigando() const {
    return estaBrigando;
}

void Animal::setEstaBrigando(bool estaBrigando) {
    Animal::estaBrigando = estaBrigando;
}

Animal::~Animal() {
}

bool Animal::isEstaVivo() const {
    return estaVivo;
}

void Animal::setEstaVivo(bool estaVivo) {
    Animal::estaVivo = estaVivo;
}


float Animal::getVelocidade() const {
    return velocidade;
}

void Animal::setVelocidade(float velocidade) {
    Animal::velocidade = velocidade;
}


#pragma clang diagnostic pop