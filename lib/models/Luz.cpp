//
// Created by antonio on 05/03/17.
//

/**
 * nao comentar nada sobre a luz no relatorio por enquanto.
 * Classe que define a luz, ativa, e desenha o "sol".
 *
 */

#include "../../include/Luz.h"
#include <vector>
#include <GL/freeglut.h>

using namespace std;

/**
 * ativa o zbuffer, e sombreamento suave
 * e chama as função necessaria para ativar a luz
 */

Luz::Luz(int x, int y, int altura, vector<vector<char>> &mundo) {
    this->luz1 = x;
    this->luz2 = y;
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    iluminar(luz1, luz2, altura, mundo);
}

/**
 * o metodo iluminar esta faltando coisas
 *
 * @param x variavel que vai dizer se a luz 1 esta ligada ou nao
 * @param y variavel que vai dizer se a luz 2 esta ligada ou nao
 *
 * decide se a Luz estara ligada ou desligada
 */

void Luz::iluminar(int x, int y, int altura, vector<vector<char>> &mundo) {
    if (x == 1) {
        cLuz1();
        glEnable(GL_LIGHT0);
    }
    if (y == 1) {
        cLuz2(altura, mundo);
        glEnable(GL_LIGHT1);
    }
}

/**
 * desenha o objeto ("Sol") de maneira de que a fonte de luz fica dentro do objeto
 */

void Luz::desenharLuz(int altura) {
    glPushAttrib(GL_LIGHTING_BIT);
    GLfloat luz_pontual[] = {0.3, 0.5, 0.5, 1.0};
    GLfloat mat_diffuse[] = {0.0, 1.0, 0.0, 1.0};
    GLfloat mat_emission[] = {1.0, 1.0, 0.0, 1.0};

    //atribui características ao material
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialfv(GL_FRONT, GL_EMISSION, mat_emission);

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_diffuse);

    glPushMatrix();
    //glTranslatef(luz_pontual[0], luz_pontual[1], luz_pontual[2]);
    glTranslatef(0, altura, 0);
    glEnable(GL_LIGHTING);
    // glColor3f (1.0, 1.0, 0.0);
    glutSolidSphere(5, 50, 50);
    glDisable(GL_LIGHTING);

    glPopAttrib();
    glPopMatrix();
}

/**
 * define como a fonte de luz 1 vai ser e sua posição
 */

void Luz::cLuz1() {
    GLfloat light0_position[] = {0.0, 0.0, 0.0, 0.0};
    GLfloat light0_diffuse[] = {1, 1, 1, 1};
    GLfloat light0_specular[] = {1.0, 1.0, 1.0, 1.0};
    //atribui características para a fonte de luz 0
    //cor padrão: branco
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light0_specular);

}

/**
 * define como a fonte de luz 2 vai ser e sua posição
 */

void Luz::cLuz2(int altura, vector<vector<char>> &mundo) {
    GLfloat luz_pontual[] = {(float) mundo.size(), (float) altura, (float) mundo[0].size(), 1.0};
    GLfloat light1_diffuse[] = {1, 1, 1, 1.0};
    GLfloat light1_specular[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat light1_ambient[] = {0.2, 0.2, 0.2, 1.0};

    //atribui as características para a fonte de luz 1
    //(experimentem remover alguns dos componentes abaixo)
    glLightfv(GL_LIGHT1, GL_POSITION, luz_pontual);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);


}

int Luz::getluz1() const {
    return luz1;
}

void Luz::setluz1(int luz1) {
    Luz::luz1 = luz1;
}

int Luz::getluz2() const {
    return luz2;
}

void Luz::setluz2(int luz2) {
    Luz::luz2 = luz2;
}

/**
 * ainda nao decidido
 * ????
 */

void Luz::propriedade() {
    GLfloat luz_pontual[] = {0.3, 0.5, 0.5, 1.0};
    glLightfv(GL_LIGHT1, GL_POSITION, luz_pontual);
}