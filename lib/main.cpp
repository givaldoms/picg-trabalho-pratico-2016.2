//Compilar: g++ -o main main.cpp -lGL -lGLU -lglut
//rodar: ./main

#include "../include/main.h"

//Define o tamanho máximo que cada lago pode ter.

static GLfloat spin = 0.0;
using namespace std;

CameraController *camera;
NaturezaController *natureza;
Luz *luz;

vector<Animal> zebras;
vector<Animal> leoes;

vector<Planta> plantas;
vector<vector<char>> mundo;
ArquivoController arquivo;

void init() {
    arquivo = *new ArquivoController();
    arquivo.CarregarArquivo("../entrada.txt");

    int cenaX = stoi(arquivo.getCena()[1]) * 10;
    int cenaZ = stoi(arquivo.getCena()[3]) * 10;
    int porcentagemIlha = stoi(arquivo.getIlha()[1]);
    int porcentagemLagos = stoi(arquivo.getLagos()[1]);
    int nZebras = stoi(arquivo.getZebras()[1]);
    int taxaZebra = stoi(arquivo.getZebras()[2]);
    int nLeoes = stoi(arquivo.getLeoes()[1]);
    int taxaLeoes = stoi(arquivo.getLeoes()[2]);
    int nPlantas = stoi(arquivo.getPlantas()[1]);
    int taxaPlantas = stoi(arquivo.getPlantas()[2]);

    //Instancia as posições da matriz mundo
    mundo.resize((unsigned long) cenaZ);//tamanho em z
    for (int i = 0; i < mundo.size(); i++) {
        mundo[i].resize((unsigned long) cenaX);//tamanho em x
    }
    //Define a cor de limpeza
    //glClearColor(0.22765, 0.56470, 1.0, 0.0);
    glClearColor(0, 0, 1.0, 0.0);

    glPolygonMode(GL_BACK, GL_LINE);
    glShadeModel(GL_SMOOTH);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(90, (GLfloat) cenaX / (GLfloat) cenaZ, 0.1f, 1000);

    camera = new CameraController(mundo);
    luz = new Luz(1, 1, stoi(arquivo.getCena()[2]), mundo);
    natureza = new NaturezaController(porcentagemIlha);
    natureza->geraOceano(mundo);
    natureza->geraIlha(mundo);

    criarLagos(porcentagemLagos);

    criarAnimais(nZebras, 0, taxaZebra);//Criando zebra
    criarAnimais(nLeoes, 1, taxaLeoes);//Criando leão

    criarPlantas(nPlantas, taxaPlantas);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

}

void criarLagos(float porcentagemLagos) {
    srand((unsigned int) time(nullptr));
    int areaLagos = (int) (natureza->getAreaIlha(mundo) * (porcentagemLagos / 100));
    int areaLagosCont = 0;
    int tamMaxLagos = natureza->getAreaIlha(mundo) / 50;

    do {
        int xr = (int) (rand() % mundo[0].size());
        int zr = (int) (rand() % mundo.size());

        if (mundo[xr][zr] == 'i') {
            Lago l = *new Lago(xr, zr);
            int lRestantes = areaLagos - areaLagosCont;

            while (lRestantes > tamMaxLagos ? l.cresceLago(mundo, tamMaxLagos) :
                   l.cresceLago(mundo, lRestantes));

            l.gerarLago(mundo);

            areaLagosCont += l.getArea();
            lagos.push_back(l);
        }

    } while (areaLagosCont <= areaLagos);


}

/**
 * Cria instâncias definidas de animais
 *
 * @param nAnimais Números de animais a serem criados
 * @param tipo Tipo do animal a ser criado (1 = leao, 0 = zebra)
 * @param taxaDiminuicao Taxa de perda de massa desse animal
 */
void criarAnimais(int nAnimais, short tipo, int taxaDiminuicao) {
    int x, z;
    for (int i = 0; i < nAnimais; i++) {
        srand((unsigned int) time(NULL));

        Animal a = *new Animal(tipo, taxaDiminuicao);

        do {
            x = (int) (rand() % (mundo[0].size() - 1));
            z = (int) (rand() % (mundo.size() - 1));

            a.setSentidoX(x);
            a.setSentidoZ(z);

        } while (!a.gerar(mundo));

        if (tipo == 1) {
            leoes.push_back(a);
        } else {
            zebras.push_back(a);
        }

    }

}

/**
 * Cria instâncias definidas de plantas
 *
 * @param nPlantas Número de plantas a serem a criada
 * @param taxaCrescimento Taxa de crescimento das plantas
 */
void criarPlantas(int nPlantas, int taxaCrescimento) {// chanudo criou
    int x, z;
    for (int i = 0; i < nPlantas; i++) {

        srand((unsigned int) time(NULL));

        Planta p = *new Planta(taxaCrescimento);

        do {
            x = (int) (rand() % (mundo.size() - 1));
            z = (int) (rand() % (mundo[0].size() - 1));

//            cout << x << " " << z << endl;

            p.setPosicaoX(x);
            p.setPosicaoZ(z);

        } while (!p.gerar(mundo));

        plantas.push_back(p);

    }
}

/**
 * Calcula a chence da zebra ganhar uma briga com um leão
 *
 * @param zebra Animal zebra que está brigando
 * @param leao Animal leão que está brigando
 * @return a chances da zebra ganhar a briga
 */
float chanceZebra(Animal zebra, Animal leao) {
    return leao.getMassa() / zebra.getMassa() * 100;
}

/**
 * Busca a zebra que está brigando
 *
 * @return posição no array de zebras da zebra que está brigando
 */
int getZebraBrigando() {

    for (int i = 0; i < zebras.size(); i++) {
        if (zebras[i].isEstaBrigando()) {
            return i;
        }
    }

    return -1;
}

void display() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glClear(GL_COLOR_BUFFER_BIT);

    //Altera a posição da Câmera
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(
            camera->getObx(),
            camera->getOby(),
            camera->getObz(),
            camera->getObx() + camera->getAlx(),
            camera->getOby() + camera->getAly(),
            camera->getObz() + camera->getAlz(),
            0.0,
            1.0,
            0.0);

    luz->propriedade();
    glPushMatrix();
    glPushAttrib(GL_CURRENT_BIT);

    luz->desenharLuz(stoi(arquivo.getCena()[2]));

    for (auto &&l : lagos) {
        l.gerarLago(mundo);
    }

    // natureza->geraNatureza(mundo);
    natureza->desenhaNatureza(mundo);
    //natureza->desenhaVerticesCentrais(mundo);

    //Desenhas as plantas
    //for (auto &&p : plantas) {

    for (int i = 0; i < plantas.size(); ++i) {
        plantas[i].desenha(mundo);
        if (plantas[i].getMassa() > 100) {
            int r = (rand() % 8) + 1;
            criarPlantas(1, plantas[i].getTaxaCrescimento());
            plantas[i].setMassa(plantas[i].getMassa() / (r + 1));

            break;
        } else if (plantas[i].getMassa() < 0.01) {
            plantas.erase(plantas.begin() + i);

        }

    }

    //Movimenta as zebras
    for (auto &&z : zebras) {
        z.movimentar(mundo);

        if (zebras.size() + leoes.size() < 15) {

            if (z.getMassa() > 2500) {
                int r = (rand() % 3) + 1;
                z.setMassa(z.getMassa() / (r + 1));
                criarAnimais(1, 0, z.getTaxaDiminuicao());
                //z.reproduzirZebra(mundo, zebras, r);
            }
        }
    }

    //Percorre o array de leoẽs
    for (int i = 0; i < leoes.size(); i++) {
        int pZebra = 0;

        leoes[i].movimentar(mundo);//movimenta cada leão

        //Um leão está brigando
        if (leoes[i].isEstaBrigando()) {
            leoes[i].setEstaBrigando(false);

            try {

                //valor aleatório usado para calcular a chances da zebra ganhar a briga
                int r = (int) (random() % 101) + 1;

                pZebra = getZebraBrigando();//busca a zebra que está brigando
                zebras[pZebra].setEstaBrigando(false);//altera estado de briga da zebra

                //se massa do leão for maior ou a zebra perder a briga
                if (leoes[i].getMassa() > zebras[pZebra].getMassa() ||
                    chanceZebra(zebras[pZebra], leoes[i]) < r) {
                    //cout << "zebra morrendo" << endl;
                    zebras[pZebra].setEstaVivo(false);//a zebra morre
                    leoes[i].setMassa(leoes[i].getMassa() * 2);//dobra a massa do leão
                    break;

                } else {
                    //a zebra ganhou a briga
                    cout << "Uma zebra escapou da morte por muito pouco." << endl;
                    //manda a zebra para outra direção
                    zebras[pZebra].setVelocidade(zebras[pZebra].getVelocidade() * -1);
                    //leão perde massa
                    leoes[i].setMassa(
                            zebras[pZebra].getMassa() - leoes[i].getMassa());

                }
            } catch (exception &e) {
                cout << "Erro na briga" << endl;
            }

        }
    }

    //Remove da matriz se o leão morreu de fome
    for (int i = 0; i < leoes.size(); i++) {
        if (leoes[i].getMassa() <= 0) {//morreu de fome
            //cout << "1 leao morreu de fome." << endl;
            leoes.erase(leoes.begin() + i);
        }


        if (leoes[i].getMassa() > 3500) {
            int r = (rand() % 2) + 1;
            leoes[i].setMassa(leoes[i].getMassa() / (r + 1));
            leoes[i].reproduzirLeao(mundo, leoes, r);
        }
    }


    //Remove da matriz se a zebra morreu de fome ou em uma briga
    for (int i = 0; i < zebras.size(); i++) {
        if (zebras[i].getMassa() <= 0) {//morreu de fome
            //cout << "1 zebra morreu de fome." << endl;
            zebras.erase(zebras.begin() + i);
        } else if (!zebras[i].isEstaVivo()) {//está morta
            cout << "Uma zebra morreu em briga com um leão." << endl;
            zebras.erase(zebras.begin() + i);
        }
    }

    //retira cabeça da pilha de atributos
    glPopAttrib();

    //retira a matriz de transformação da cabeça da pilha
    glPopMatrix();

    // animais[0].setVelocidade(0);
    //troca de buffers, o flush é implícito aqui
    glutSwapBuffers();

    //imprimeMundo(mundo);

}

void imprimeMundo(vector<vector<char>> mundo) {
    cout << "=======================================================" << endl;
    for (int i = 0; i < mundo.size(); i++) {
        for (int j = 0; j < mundo[i].size(); j++) {
            cout << mundo[i][j] << " ";
        }
        cout << endl;
    }
    cout << "======================================================" << endl;
}

//função que incrementa o ângulo de rotação
void spinDisplay(void) {
    spin = (GLfloat) (spin + 2.0);
    if (spin > 360.0)
        spin = (GLfloat) (spin - 360.0);

    glutPostRedisplay();
}

//função chamada quando a tela é redimensionada
void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"

//evento que trata clique do mouse
void mouse(int button, int state, int x, int y) {
    switch (button) {
        case GLUT_LEFT_BUTTON:
            if (state == GLUT_DOWN)
                //faz com que a função spinDisplay seja chamada sempre que o
                //programa estiver ocioso
                glutIdleFunc(spinDisplay);
            break;
        case GLUT_RIGHT_BUTTON:
            if (state == GLUT_DOWN)
                //faz com que nenhuma função seja chamada quando o
                //programa estiver ocioso
                glutIdleFunc(NULL);
            break;
        default:
            break;
    }
}

void keyPressed(unsigned char key, int x, int y) {

    if (key == 'p') {
        printf("%f\n", camera->getAlx());
        printf("%f\n", camera->getAly());
        printf("%f\n", camera->getAlz());
        return;
    }

    if (abs(camera->getAly()) >= abs(camera->getAlx()) &&
        abs(camera->getAly()) >= abs(camera->getAlz())) {
        switch (key) {
            case 'w':
                camera->setOby(
                        camera->getOby() - camera->getCameraVelocidade());
                glutPostRedisplay();
                break;

            case 's':
                camera->setOby(
                        camera->getOby() + camera->getCameraVelocidade());
                glutPostRedisplay();
                break;
            default:
                break;
        }
        return;
    } else if (camera->getAlx() >= 0.40) {
        switch (key) {
            case 'w':
                camera->setObx(
                        camera->getObx() + camera->getCameraVelocidade());
                glutPostRedisplay();
                break;

            case 's':
                camera->setObx(
                        camera->getObx() - camera->getCameraVelocidade());
                glutPostRedisplay();
                break;
            default:
                break;

        }
        return;
    } else if (camera->getAlx() <= -0.40) {
        switch (key) {
            case 'w':
                camera->setObx(
                        camera->getObx() - camera->getCameraVelocidade());
                glutPostRedisplay();
                break;

            case 's':
                camera->setObx(
                        camera->getObx() + camera->getCameraVelocidade());
                glutPostRedisplay();
                break;
            default:
                break;

        }
        return;

    } else if (camera->getAlz() >= 0) {
        switch (key) {
            case 'w':
                camera->setObz(
                        camera->getObz() + camera->getCameraVelocidade());
                glutPostRedisplay();
                break;

            case 's':
                camera->setObz(
                        camera->getObz() - camera->getCameraVelocidade());
                glutPostRedisplay();
                break;
            default:
                break;

        }
    } else {
        switch (key) {
            case 'w':
                camera->setObz(
                        camera->getObz() - camera->getCameraVelocidade());
                glutPostRedisplay();
                break;

            case 's':
                camera->setObz(
                        camera->getObz() + camera->getCameraVelocidade());
                glutPostRedisplay();
                break;
            default:
                break;
        }
    }

}

void keySpecial(int key, int x, int y) {
    glPushMatrix();

    switch (key) {
        case GLUT_KEY_DOWN:
            camera->setAly(camera->getAly() - camera->getCameraVelocidade());
            glutPostRedisplay();
            break;

        case GLUT_KEY_UP:
            if (camera->getAly() < 0.0) {
                camera->setAly(
                        camera->getAly() + camera->getCameraVelocidade());
                glutPostRedisplay();
            }
            break;

        case GLUT_KEY_LEFT:
            camera->setAlx(camera->getAlx() + camera->getCameraVelocidade());
            glutPostRedisplay();
            break;

        case GLUT_KEY_RIGHT:
            camera->setAlx(camera->getAlx() - camera->getCameraVelocidade());
            glutPostRedisplay();
            break;

        case GLUT_KEY_F1:
            camera->setAlz(camera->getAlz() - camera->getCameraVelocidade());
            glutPostRedisplay();
            break;

        case GLUT_KEY_F2:
            camera->setAlz(camera->getAlz() + camera->getCameraVelocidade());
            glutPostRedisplay();
            break;

        default:
            break;
    }
    glPopMatrix();


}

int main(int argc, char **argv) {

    glutInit(&argc, argv);
    //glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(700, 700);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);

    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);

    glutKeyboardFunc(keyPressed);
    glutSpecialFunc(keySpecial);

    glutMainLoop();

    return 0;
}
