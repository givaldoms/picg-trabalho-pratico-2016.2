//
// Created by givaldo on 23/02/17.
//

/**
 * Classe que define os valores iniciais da camera
 *
 */


#include <GL/freeglut.h>
#include <vector>
#include "../../include/CameraController.h"

using namespace std;

/**
 *
 * @param mundo
 * construtor da camera, onde vai definir os valores iniciais da camera
 *
 */

CameraController::CameraController(vector<vector<char>> mundo) {

    this->alx = 0;
    this->aly = (float) -0.2;
    this->alz = (float) 0.1;

    this->obx = mundo[0].size() / 2;
    this->oby = (float) (mundo.size() * 0.8);
    this->obz = (float) (mundo.size() * 0.3);
    this->cameraVelocidade = 0.6;


}

float CameraController::getObx() const {
    return obx;
}

void CameraController::setObx(float obx) {
    CameraController::obx = obx;
}

float CameraController::getOby() const {
    return oby;
}

void CameraController::setOby(float oby) {
    CameraController::oby = oby;
}

float CameraController::getObz() const {
    return obz;
}

void CameraController::setObz(float obz) {
    CameraController::obz = obz;
}

float CameraController::getAlx() const {
    return alx;
}

void CameraController::setAlx(float alx) {
    CameraController::alx = alx;
}

float CameraController::getAly() const {
    return aly;
}

void CameraController::setAly(float aly) {
    CameraController::aly = aly;
}

float CameraController::getAlz() const {
    return alz;
}

void CameraController::setAlz(float alz) {
    CameraController::alz = alz;
}

float CameraController::getCameraVelocidade() const {
    return cameraVelocidade;
}

void CameraController::setCameraVelocidade(float cameraVelocidade) {
    CameraController::cameraVelocidade = cameraVelocidade;
}


