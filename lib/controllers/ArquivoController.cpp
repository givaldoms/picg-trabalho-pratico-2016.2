
/**
 * Created by Givaldo Marques e Antônio Bispo on 05/03/17.
 *
 * Carrega arquivos de entrada e
 * salva nos respectivos atributos
 *
 */

#include <sstream>
#include "../../include/ArquivoController.h"

using namespace std;


ArquivoController::ArquivoController() {

}

/**
 * Carrega o arquivo de entrada contendo as informações
 * necessárias para construir o cenário.
 *
 * @param nomeArquivo Caminho do arquivo de entrada
 */
void ArquivoController::CarregarArquivo(string nomeArquivo) {

    ifstream in(nomeArquivo);
    string texto;
    vector<string> linhas;

    while (in.good()) {
        texto.push_back((char) in.get());
    }

    linhas = split(texto, '\n');//separa o arquivo de entrada por linha

    this->cena = split(linhas[0], ' ');
    this->ilha = split(linhas[1], ' ');
    this->lagos = split(linhas[2], ' ');
    this->leoes = split(linhas[3], ' ');
    this->zebras = split(linhas[4], ' ');
    this->plantas = split(linhas[5], ' ');

}

vector<string> ArquivoController::split(const string &s, char delim) {
    stringstream ss(s);
    string item;
    vector<string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}

const vector<string> &ArquivoController::getCena() const {
    return cena;
}

const vector<string> &ArquivoController::getIlha() const {
    return ilha;
}

const vector<string> &ArquivoController::getLagos() const {
    return lagos;
}

const vector<string> &ArquivoController::getLeoes() const {
    return leoes;
}

const vector<string> &ArquivoController::getZebras() const {
    return zebras;
}

const vector<string> &ArquivoController::getPlantas() const {
    return plantas;
}
