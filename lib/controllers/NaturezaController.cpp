//
// Created by givaldo on 27/02/17.
//

/*
 *  Classe responsavel por gerar e desenhar o oceano e ilha, alem de decidir suas cores.
 */

#include <GL/gl.h>
#include <iostream>
#include <vector>
#include <cmath>

#include "../../include/NaturezaController.h"
#include "../models/Lago.cpp"

using namespace std;

vector<Lago> lagos;

NaturezaController::NaturezaController(float porcentagemIlha) : porcentagemIlha(
        porcentagemIlha) {

    this->modeloChao = Modelo::carregarObj(
            (char *) "../modelos/chao/chao.obj",
            (char *) "../modelos/chao/chao.bmp");

}

/**
 * @param mundo
 * Desenha os vertices centrais para servir de base
 */
void NaturezaController::desenhaVerticesCentrais(vector<vector<char>> &mundo) {

    int origemX = (int) (mundo.size() / 2);
    int orgemZ = (int) (mundo[0].size() / 2);

    glPushMatrix();
    glBegin(GL_LINES);
    glColor3f(1, 0.0, 0.0);
    glVertex3f(origemX, 0, orgemZ);
    glVertex3f(mundo.size() / 2, 0, 0);

    glColor3f(0.0, 1, 0.0);
    glVertex3f(origemX, 0, orgemZ);
    glVertex3f(mundo[0].size() / 2, mundo.size() / 2, mundo.size() / 2);

    glColor3f(0.0, 0.0, 1);
    glVertex3f(origemX, 0, orgemZ);
    glVertex3f(0, 0, mundo[0].size() / 2);
    glEnd();
    glPopAttrib();
    glPopMatrix();

}

/**
 * Gera na matriz mundo as posições relacionadas a ilha
 *
 * @param tamMundo tamanho do mundo
 * @param pocentegemIlha porcentagem do mundo que a ilha deve ocupar
 */
void NaturezaController::geraIlha(vector<vector<char>> &mundo) {

    if (&mundo == nullptr) {
        cout << "Erro ao gerar a ilha!" << endl;

    } else {

        int margemX = (int) (mundo[0].size() * sqrt(this->porcentagemIlha) / 10);
        int margemZ = (int) (mundo.size() * sqrt(this->porcentagemIlha) / 10);

        float x1 = mundo[0].size() / 2 - margemX / 2;
        float x2 = mundo[0].size() / 2 + margemX / 2;
        float z1 = mundo.size() / 2 - margemZ / 2;
        float z2 = mundo.size() / 2 + margemZ / 2;

        for (int i = (int) z1; i < (int) z2; i++) {
            for (int j = (int) x1; j < (int) x2; j++) {
                mundo[i][j] = 'i';
            }
        }
    }
}

/**
 * Gera na matriz mundo as posições relacionadas ao oceano
 *
 * @param mundo matriz mundo
 */
void NaturezaController::geraOceano(vector<vector<char>> &mundo) {

    for (int i = 0; i < mundo.size(); i++) {
        for (int j = 0; j < mundo[i].size(); j++) {
            mundo[i][j] = '~';
        }
    }

}

/**
 * Decide de qual cor o piso vai ser
 *
 * @param item valor de uma posiçao x e y do mundo
 *
 */

Cor NaturezaController::getItemCor(char item) {

    switch (item) {
        case '~':
            return *new Cor(58, 144, 255);

        case 'i':
            return *new Cor(0, 128, 0);

        case '*':
            return *new Cor(50, 205, 153);

        case 'L':
            return *new Cor(0, 128, 0);

        case 'Z':
            return *new Cor(0, 128, 0);

        case 'P':
            //return *new Cor(0, 0, 0);
            return *new Cor(0, 128, 0);

        default:
            return *new Cor(0, 0, 0);
    }
}

/**
 * Mapeia para o OpenGL a matriz mundo
 *
 * @param mundo matriz mundo
 */
void NaturezaController::desenhaNatureza(vector<vector<char>> mundo) {

    for (int i = 0; i < mundo.size() - 1; i++) {
        for (int j = 0; j < mundo[i].size() - 1; ++j) {
            Cor cor = getItemCor(mundo[i][j]);
//
//            if (mundo[i][j] == 'i' ) {
//                glEnable(GL_TEXTURE_2D);
//                glPushMatrix();
//                glEnable(GL_LIGHTING);
//                glTranslated(i, 0, j);
//                this->modeloChao->desenhar();
//                glDisable(GL_LIGHTING);
//                glPopMatrix();
//                glPopAttrib();
//                glDisable(GL_TEXTURE_2D);
//
//            }
            glColor3ub(cor.getR(), cor.getG(), cor.getB());
            //glEnable(GL_LIGHTING);
            glPushMatrix();

            glBegin(GL_QUADS);
            glVertex3f(i, 0.0f, j);
            glVertex3f(i, 0.0f, j + 1);
            glVertex3f(i + 1, 0.0f, j + 1);
            glVertex3f(i + 1, 0.0f, j);
            glEnd();
            glPopAttrib();
            glPopMatrix();
            //glDisable(GL_LIGHTING);
            //}



        }
    }
}

/**
 * @param mundo matriz mundo
 * @return area da ilha
 */
int NaturezaController::getAreaIlha(vector<vector<char>> mundo) {
    float pIlha = this->porcentagemIlha / 100;

    return (int) ((mundo[0].size() * mundo.size()) * pIlha);
}


